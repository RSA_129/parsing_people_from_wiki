import re
import time
import urllib.parse

import hashlib
from get_info_to_send import extract_persons_data
from bs4 import BeautifulSoup
from html_get import html_get
from config import SLEEPING_TIME
from email_sender import create_gmail



def make_backup_description(text):
    text = re.sub(r'\[.*?\]', '', text)
    text = text.replace(u'\xa0', '')
    text += '/ddd/'
    return text

def get_people(month, processed_people: set, send_to_email_people: list, data):
    soup = BeautifulSoup(data, 'html.parser')
    nw_month = soup.find_all('h2')[1].text
    if nw_month != month:
        clear_processed_people()
    month = nw_month
    content_container = soup.find(class_='mw-body-content mw-content-ltr').find('h2')
    while True:
        if content_container.name == 'ul':
            for person in content_container.find_all('li'):
                link = person.a.get("href")
                link = urllib.parse.unquote(link)
                link = link.split('/')[-1]
                if link.startswith('index.php'):
                    link = make_backup_description(person.text)
                if link and link not in processed_people:
                    send_to_email_people.append(link)

        content_container = content_container.next_sibling
        if content_container.text == 'Previous months' or not content_container:
            break
    return month, send_to_email_people


def cash_processed_people(processed_people: list):
    with open('cashed_processed_people.txt', 'a+') as f:
        if processed_people:
            f.write('\n')
            f.write('\n'.join(processed_people))

def clear_processed_people():
    with open('cashed_processed_people.txt', 'w') as f:
        f.write('')

def get_processed_people():
    with open('cashed_processed_people.txt') as f:
        return f.read().strip().split('\n')


def cash_month(month: str):
    with open('cashed_month.txt', 'w') as f:
        f.write(month.strip())

def get_month():
    with open('cashed_month.txt') as f:
        return f.read().strip()

def cash_page(page):
    with open('cashed_page.txt', 'w') as f:
        f.write(page)

def get_cashed_page():
    with open('cashed_page.txt') as f:
        return f.read()

def main():
    BASE_URL = 'https://en.wikipedia.org/wiki/'
    URL = f'https://en.wikipedia.org/wiki/Deaths_in_2023'

    month = get_month()
    send_to_email_people = []
    gmail = create_gmail()

    while True:
        data = html_get(URL)
        if data:
            currentHash = hashlib.sha224(data.encode()).hexdigest()
            if get_cashed_page() != currentHash:
                processed_people = set(get_processed_people())
                month, send_to_email_people = \
                    get_people(month, processed_people, send_to_email_people, data)

                extract_persons_data(send_to_email_people, BASE_URL, gmail)
                cash_processed_people(send_to_email_people)
                cash_month(month)
                cash_page(currentHash)


                send_to_email_people.clear()
        else:
            print(f'Unnable to connect to {URL}')
        time.sleep(SLEEPING_TIME)




if __name__ == '__main__':
    main()

