import os


EMAIL_LOGIN = os.environ['email_login']
EMAIL_PASSWORD = os.environ['email_password']
SEND_TO_EMAIL = os.environ['email_receiver']

SLEEPING_TIME = 2
