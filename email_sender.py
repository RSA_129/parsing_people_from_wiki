import smtplib
from config import EMAIL_LOGIN, EMAIL_PASSWORD, SEND_TO_EMAIL

def create_gmail():
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.login(EMAIL_LOGIN, EMAIL_PASSWORD)
    return s


def send_mail(s, txt: str):
    txt = txt.encode('utf-8')
    s.sendmail(EMAIL_LOGIN, SEND_TO_EMAIL, txt)

