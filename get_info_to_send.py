import time
import unicodedata

from bs4 import BeautifulSoup
import wikipedia
import re

from email_sender import send_mail
from html_get import html_get
from config import SLEEPING_TIME


def back_up_search(link):
    data = html_get(link)
    text = ''
    if data:
        soup = BeautifulSoup(data, 'html.parser')
        content = soup.find(class_='mw-parser-output')
        content = content.find_all('p')[1]
        text = content.text
    else:
        print(f'Unnable to connect to {link}')
    return text

def check_lan(url):
    data = html_get(url)
    if data:
        lan = ''
        soup = BeautifulSoup(data, 'html.parser')
        content = soup.find('li', class_=re.compile(r'^interlanguage-link interwik'))
        while content and content.text.strip():
            info = content.find('a').get('lang')
            if info == 'ru':
                lan = 'ru'
                break
            content = content.next_sibling
        if not lan:
            lan = 'en'
        return lan
    else:
        print(f'Unnable to connect to {url}')

def beautify_desc(txt):
    def replace_characters(original, new, str):
        for i in range(len(original) - 1):
            str = str.replace(original[i], new[i])
        return str

    def remove_accents(input_str):
        nfkd_form = unicodedata.normalize('NFKD', input_str)
        return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


    char_preserve = ["й", "ё", "Ё", "Й"]
    placeholders = ["}", "#", "<", ">"]

    temp = replace_characters(char_preserve, placeholders, txt)
    temp = remove_accents(temp)
    res = replace_characters(placeholders, char_preserve, temp)
    return res



def extract_persons_data(titles, base_url, gmail):
    for title in titles:
        if title.endswith('/ddd/'):
            page_content = title[:-5]
        else:
            lan = check_lan(base_url + title)
            wikipedia.set_lang(lan)
            try:
                page_content = wikipedia.page(title)
                page_content = page_content.summary
            except wikipedia.PageError:
                page_content = back_up_search(base_url + title)
        page_content = re.sub(r'\[.*?\]', '', page_content)
        page_content = beautify_desc(page_content)
        send_mail(gmail, page_content)
        time.sleep(SLEEPING_TIME)
